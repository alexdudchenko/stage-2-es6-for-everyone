import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  if (defender.getBlockPower > attacker.getHitPower) {
    return 0;
  }

  return attacker.getHitPower - defender.getBlockPower;
}

export function getHitPower(fighter) {
  return fighter.getHitPower;
}

export function getBlockPower(fighter) {
  return fighter.getBlockPower;
}
